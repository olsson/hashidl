default: moo

moo:
	@apt-get moo moo moo

install:
	ansible-playbook --ask-become-pass --inventory=localhost, --connection=local ./hashidl.yaml

.PHONY: default moo install
