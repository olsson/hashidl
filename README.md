# hashidl

An Ansible playbook to download, verify and install/upgrade
[HashiCorp][1] tools on an Ubuntu workstation.


## Usage

1. Compare _./roles/base/files/hashicorp.gpg_ against https://www.hashicorp.com/security/.
2. Inspect the playbook's roles, to ensure that they do what you think think they do.
3. `make install`
4. Profit.


[1]: https://www.hashicorp.com/
